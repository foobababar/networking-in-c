// references
// https://www.youtube.com/watch?v=mStnzIEprH8

#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<unistd.h>

#include<sys/socket.h>
#include<sys/types.h>

#include<netinet/in.h>
#include<arpa/inet.h>

int main(int argc, char *argv[]) {
    char address[50];
    strcpy(address, argv[1]);

    int client_socket;
    client_socket = socket(AF_INET, SOCK_STREAM, 0);
    
 

    //
    struct sockaddr_in remote_address;
    remote_address.sin_family = AF_INET;
    remote_address.sin_port = htons(80);
    inet_aton(address, &remote_address.sin_addr.s_addr);

    connect(client_socket, (struct sockaddr *) &remote_address, sizeof(remote_address));

    char request[200];
    sprintf(request, "GET /%s HTTP/1.1\r\nHost: %s\r\nContent-Type: text/plain\r\n\r\n", "index.html", "www.google.com");
    char response[4096];

    send(client_socket, request, sizeof(request), 0);
    recv(client_socket, &response, sizeof(response), 0);

    printf("resp : %s", response);
    close(client_socket);


    return 0;
}
