// code is from youtube channel Eduonix Learning Solutions
// link : https://www.youtube.com/watch?v=LtXEMwSG5-8&t=2124s


// use like so : gcc client.c -o client
// ./client <IP ADDRESS>


#include<stdio.h>
#include<stdlib.h>
#include<unistd.h> // for close()

#include<sys/types.h>
#include<sys/socket.h>

#include<netinet/in.h>

int main() {

    // 1) create socket
    int network_socket;
    network_socket = socket(AF_INET, SOCK_STREAM, 0);



    // 2) setup socket
    struct sockaddr_in server_address;
    server_address.sin_family = AF_INET;
    server_address.sin_port = htons(9002);
    server_address.sin_addr.s_addr = INADDR_ANY;


    // 3) connect socket
    int connection_status = connect(network_socket, (struct sockaddr *) &server_address, sizeof(server_address));
    if (connection_status == -1){
        printf("error connecting to remote socket\n"); 
    }


    // 4) get server response
    char server_response[256];
    recv(network_socket, &server_response, sizeof(server_response), 0);
    printf("server sent : %s\n", server_response);

    close(network_socket);

    




    return 0;
}
