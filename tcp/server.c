// code is from youtube channel Eduonix Learning Solutions
// link : https://www.youtube.com/watch?v=LtXEMwSG5-8&t=2124s

// use like so :
// gcc server.c -o server
// ./server 
// then connect from web browser localhost:8080



#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>

#include<sys/types.h>
#include<sys/socket.h>

#include<netinet/in.h>

int main() {
	char server_message[256] = "hello world from server !";

	// 1) create socket
	int server_socket;
	server_socket = socket(AF_INET, SOCK_STREAM, 0);


	// 2) setup socket
	struct sockaddr_in server_address;
	server_address.sin_family = AF_INET;
	server_address.sin_port = htons(8080);
	server_address.sin_addr.s_addr = INADDR_ANY;


	// 3) bind socket
	bind(server_socket, (struct sockaddr*) &server_address, sizeof(server_address));


	// 4) listen
	listen(server_socket, 5);

	int client_socket;
	client_socket = accept(server_socket, NULL, NULL);

	//
	send(client_socket, server_message, sizeof(server_message), 0);

	//
	close(server_socket);
	

	return 0;
}
